#-*- coding: utf-8 -*-

# 경로지정 후, 있는 지 없는 지 확인하기

from os import listdir
from os.path import isfile, join
import os
from thebill_database.database import Session
from thebill_database.database.models.thebill import *
from thebill_settings import path
from batch_manager.customer_process.CreateCustomerProcess import CreateCustomerProcess
from batch_manager.customer_process.DeleteCustomerProcess import DeleteCustomerProcess
from batch_manager.customer_process.ModifyCustomerProcess import ModifyCustomerProcess
from batch_manager.transfer_process.CreateTransferProcess import CreateTransferProcess
from batch_manager.customer_process.CreateCustomer import CreateCustomer
from batch_manager.customer_process.DeleteCustomer import DeleteCustomer
from batch_manager.customer_process.ModifyCustomer import ModifyCustomer
from batch_manager.transfer_process.CreateTransfer import CreateTransfer




class manager:

    defined_service = {
        "고객정보생성" : {"database" : Create_customer, "service" : CreateCustomerProcess, "enum" : CreateCustomer},
        "고객정보삭제" : {"database" : Delete_customer, "service" : DeleteCustomerProcess, "enum" : DeleteCustomer},
        "고객정보수정" : {"database" : Modify_customer, "service" : ModifyCustomerProcess, "enum" : ModifyCustomer},
        "이체신청" : {"database" : Create_transfer, "service" : CreateTransferProcess, "enum" : CreateTransfer},
        "증빙자료" : {"database" : Document, "enum" :1},
    }

    def __init__(self):
        self.file_list = []
        self.session = Session()
        self.customer_create_dict = {"database" : Create_customer, "service" : CreateCustomerProcess, "enum" : CreateCustomer, "list" : []}
        self.customer_delete_dict= {"database" : Delete_customer, "service" : DeleteCustomerProcess, "enum" : DeleteCustomer, "list" : []}
        self.customer_modify_dict= {"database" : Modify_customer, "service" : ModifyCustomerProcess, "enum" : ModifyCustomer, "list" : []}
        self.transfer_create_dict= {"database" : Create_transfer, "service" : CreateTransferProcess, "enum" : CreateTransfer, "list" : []}

    def request_service_monitor(self):
        path_list = [
            path.cms_url,
            # path.ok_url,
            # path.BASE_URL,
            # path.RECV_URL,
            # path.SEND_OK_URL,
            # path.TEMP_URL
        ]


        # 해당 path에 있는 textfile을 하나씩 읽어서 추가하기.

        for file_path in path_list:
            self._file_check_in_dir(file_path)

        self.file_list = []
        self.start_request_service()

    def _set_service_parameter(self, service, number):
        if service == "고객정보생성":
            self.customer_create_dict["list"].append(number)
        elif service == "고객정보삭제":
            self.customer_delete_dict["list"].append(number)
        elif service == "고객정보수정":
            self.customer_modify_dict["list"].append(number)
        elif service == "이체":
            pass



    def _file_check_in_dir(self, path):
        for file in listdir(path):
            splited_info = file.split('__')
            service = splited_info[2].replace('.txt', '')
            number = splited_info[0]
            self._set_service_parameter(service, number)

    def start_request_service(self):
        create_len = len(self.customer_create_dict["list"])
        delete_len = len(self.customer_delete_dict["list"])
        modify_len = len(self.customer_modify_dict["list"])

        customer_create_service = self.customer_create_dict["service"]
        customer_delete_service = self.customer_delete_dict["service"]
        customer_modify_service = self.customer_modify_dict["service"]

        if create_len > 0:
            customer_create_service(self.customer_create_dict, "request")
        if delete_len > 0:
            customer_delete_service(self.customer_delete_dict, "request")
        if modify_len > 0:
            customer_modify_service(self.customer_modify_dict, "request")

        # self.customer_delete_dict= {"database" : Delete_customer, "service" : DeleteCustomerProcess, "enum" : DeleteCustomer, "list" : []}
        # self.customer_modify_dict= {"database" : Modify_customer, "service" : ModifyCustomerProcess, "enum" : ModifyCustomer, "list" : []}
        # self.transfer_create_dict= {"database" : Create_transfer, "service" : CreateTransferProcess, "enum" : CreateTransfer, "list" : []}

    def start_response_service(self):
        pass

batch = manager()
batch.request_service_monitor()
del batch
