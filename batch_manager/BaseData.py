

class BaseData(object):
    def __init__(self, column_name, length, description, writer , starting_point, data_type, blank = ' ', default_value=''):
        if type(blank) != str: # 그냥 0을 넣으면 안됨.
            raise ValueError("올바른 값을 넣어주세요.")
        self.column_name = column_name
        self.writer = writer # writer의 값은 1 혹은 0으로 셋팅 1(업체) / 0(thebill)
        self.length = length # data_length
        self.description = description
        self.starting_point = starting_point
        self.data_type = data_type
        self.default_value = default_value
        self.final_value = ''
        self.blank = blank

    # request service type인 경우에는 writer가 업체(즉, 우리가 아닌 경우 이 값을 공백으로 처리해준다.

    def _check_name(self, db_name):
        return self.column_name == db_name

    def set_final_value(self, final_value):
        self.final_value = final_value

    def set_value_from_service_type(self):
        if self.writer == "나이스":
            self.final_value = self.length * self.blank

    def set_default_value(self):
        if len(self.default_value) > 0:
            self.final_value = self.default_value

    # def set_value_from_database(self, database):
    #     if database.name == self.column_name:
    #         self.final_value = database.value

    def _fill_blank(self):
        if type(self.final_value):
            self.final_value = str(self.final_value)

        print("column name : {} , final_value : {} final_value_len {}, len : {}, ".format(self.column_name, self.final_value, len(self.final_value), self.length))

        len_space = self.length - len(self.final_value.encode('ANSI'))

        if len_space < 0:
            raise ValueError("data string 최대값을 초과했습니다.")

        blank =  self.blank * len_space

        self.final_value = blank + self.final_value
