from batch_manager.BaseData import BaseData
from enum import Enum

class Document():
    document_1 = BaseData('작업', 1, 'N : 신규등록 , D : 기등록 삭제', '업체', 0, '1', ' ')
    document_2 = BaseData('거래사ID', 8, '거래사아이디', '업체', 1, '8', ' ')
    document_3 = BaseData('송신일자', 8, '', '업체', 9, '8', ' ')
    document_4 = BaseData('회원ID', 20, '이용기관의 신규 회원ID로 회원등록 시 전문상의 ID와 일치', '업체', 17, '20', ' ')
    document_5 = BaseData('증빙구분', 1, '1: 서면, 4 녹취', '업체', 37, '1', ' ')
    document_6 = BaseData('확장자명', 4, '서면: jpg, jpeg, gif, tif 녹취: mp3, wav', '업체', 38, '4', ' ')
    document_7 = BaseData('증빙자료크기', 7, '증빙자료의 Byte 수', '업체', 42, '7', ' ')
    document_8 = BaseData('원본파일명', 36, '증빙자료 파일 풀 네임', '업체', 49, '36', ' ')
    document_9 = BaseData('증빙_처리결과', 1, 'Y: 정상, N: 실패', '나이스', 85, '1', ' ')
    document_10 = BaseData('증빙_결과코드', 4, '2.7 결과 코드표 참조', '나이스', 86, '4', ' ')
    document_11 = BaseData('증빙_결과메세지', 38, '증빙결과메시지', '나이스', 90, '38', ' ')
    document_12 = BaseData('CR', 1, 'CR', '업체', 128, '1', ' ', '\r')
    document_13 = BaseData('LF', 1, 'LF', '업체', 129, '1', ' ', '\n')