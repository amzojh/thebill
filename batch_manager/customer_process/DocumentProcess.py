from batch_manager.BaseProcess import BaseProcess


class DeleteCustomerProcess(BaseProcess):

    def __init__(self, database, enum_class, service_type):
        self.database = database
        self.enum_class = enum_class
        self.service_type = service_type
        self.database_dict = super()._query_result_to_dict(database)


    def request_set_header(self):
        pass

    def request_set_data(self):
        pass

    def request_set_tail(self):
        pass

    def request_process(self):
        pass

    def response_set_header(self):
        pass

    def response_set_data(self):
        pass

    def response_set_tail(self):
        pass

    def response_process(self):
        pass
