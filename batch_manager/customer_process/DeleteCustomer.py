from batch_manager.BaseData import BaseData
from enum import Enum

class DeleteCustomer():
    class Head(Enum):
        customer_delete_head_1 = BaseData('Record구분', 1, 'HEADER RECORD', '고정', 0, 'C', ' ', 'H')
        customer_delete_head_2 = BaseData('거래사 ID', 8, '이용 거래사 ID', '업체', 1, 'C', ' ')
        customer_delete_head_3 = BaseData('거래사그룹여부', 1, '그룹거래사ID 사용여부', '업체', 9, 'C', ' ')
        customer_delete_head_4 = BaseData('회원신청건수', 6, '총신청건수(DATA부분건수)', '업체', 10, 'N', ' ')
        customer_delete_head_5 = BaseData('공백', 272, 'SPACE', '업체', 16, 'C', ' ')
        customer_delete_head_6 = BaseData('헤더_처리결과', 1, 'Y : 정상', '나이스', 288, 'C', ' ')
        customer_delete_head_7 = BaseData('헤더_결과코드', 4, 'H***', '나이스', 289, 'C', ' ')
        customer_delete_head_8 = BaseData('헤더_결과메세지', 30, '결과코드의 메시지', '나이스', 293, 'C', ' '),
        customer_delete_head_9 = BaseData('공백', 20, 'SPACE', '업체', 323, 'C', ' '),
        customer_delete_head_10 = BaseData('CR', 1, '', '나이스', 343, 'C', ' ', '\r')
        customer_delete_head_11 = BaseData('LF', 1, '', '나이스', 344, 'C', ' ', '\n')


    class Data(Enum):
        customer_delete_data_1 = BaseData('Record구분', 1, 'DATA RECORD', '고정', 0, 'C', ' ', 'D')
        customer_delete_data_2 = BaseData('거래사 ID', 8, '이용 거래사 ID', '업체', 1, 'C', ' ')
        customer_delete_data_3 = BaseData('거래사그룹ID', 8, '거래사그룹ID', '업체', 9, 'C', ' ')
        customer_delete_data_4 = BaseData('연번', 6, 'DATA순번(000001~ 순차적증가)', '업체', 17, 'N', ' ')
        customer_delete_data_5 = BaseData('처리내역', 1, '해지 : D', '업체', 23, 'C', ' ')
        customer_delete_data_6 = BaseData('회원ID', 20, '회원 ID', '업체', 24, 'C', ' ')
        customer_delete_data_7 = BaseData('회원명', 20, '회원성명', '업체', 44, 'C', ' ')
        customer_delete_data_8 = BaseData('공백', 223, 'SPACE', '업체', 64, 'C', ' ')
        customer_delete_data_21 = BaseData('서비스코드', 1, '은행:B,카드:C', '업체', 287, 'C', ' ')
        customer_delete_data_9 = BaseData('데이터_처리결과', 1, 'Y: 정상', '나이스', 288, 'C', ' ')
        customer_delete_data_10 = BaseData('데이터_결과코드', 4, 'D***', '나이스', 289, 'C', ' ')
        customer_delete_data_11 = BaseData('데이터_결과메세지', 30, '결과코드의 메시지', '나이스', 293, 'C', ' ')
        customer_delete_data_12 = BaseData('사용자정의', 20, '미사용시 SPACE', '업체', 323, 'C', ' ')
        customer_delete_data_13 = BaseData('CR', 1, '', '나이스', 343, 'C', ' ', '\r')
        customer_delete_data_14 = BaseData('LF', 1, '', '나이스', 344, 'C', ' ', '\n')

    class DeleteCustomerTail(Enum):
        customer_delete_tail_1 = BaseData('Record구분', 1, 'TAIL RECORD', '고정', 0, 'C', ' ', 'T')
        customer_delete_tail_2 = BaseData('거래사 ID', 8, '이용 거래사 ID', '나이스', 1, 'C', ' ')
        customer_delete_tail_3 = BaseData('거래사그룹여부', 1, '그룹거래사ID 사용여부', '나이스', 9, 'C', ' ')
        customer_delete_tail_4 = BaseData('회원신청건수', 6, '총신청건수', '나이스', 10, 'N', ' ')
        customer_delete_tail_5 = BaseData('신청성공건수', 6, '신청성공건수', '나이스', 16, 'N', ' ')
        customer_delete_tail_6 = BaseData('결과성공건수', 6, '결과성공건수', '나이스', 22, 'N', ' ')
        customer_delete_tail_7 = BaseData('공백', 260, 'SPACE', '업체', 28, 'C', ' ')
        customer_delete_tail_8 = BaseData('테일_처리결과', 1, 'Y : 정상', '나이스', 288, 'C', ' ')
        customer_delete_tail_9 = BaseData('테일_결과코드', 4, 'T***', '나이스', 289, 'C', ' ')
        customer_delete_tail_10 = BaseData('테일_결과메세지', 30, '결과코드의 메시지', '나이스', 293, 'C', ' ')
        customer_delete_tail_11 = BaseData('공백', 20, 'SPACE', '업체', 323, 'C', ' ')
        customer_delete_tail_12 = BaseData('CR', 1, '', '나이스', 343, 'C', ' ', '\r')
        customer_delete_tail_13 = BaseData('LF', 1, '', '나이스', 344, 'C', ' ', '\n')
