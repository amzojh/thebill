from batch_manager.BaseProcess import BaseProcess


class DeleteCustomerProcess(BaseProcess):

    def __init__(self, service_dict, service_type):
        super().__init__(service_dict, service_type)
        pass


    def request_set_header(self):
        pass

    def request_set_data(self):
        pass

    def request_set_tail(self):
        pass

    def request_process(self):
        pass

    def response_set_header(self):
        pass

    def response_set_data(self):
        pass

    def response_set_tail(self):
        pass

    def response_process(self):
        pass
