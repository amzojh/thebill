from batch_manager.BaseProcess import BaseProcess
from thebill_database.database.models.thebill import Customer
import os

class CreateCustomerProcess(BaseProcess):

    def __init__(self, service_dict, service_type):
        super().__init__(service_dict, service_type)
        pass


    def request_set_header(self):
        print(self.enum_class_head)
        self._fill_nice_blank(self.enum_class_head)
        self._set_default_value_to_final(self.enum_class_head)
        self._fill_blank(self.enum_class_head)
        final_str = self.create_string(self.enum_class_head)
        self._show_enum_value(self.enum_class_head)
        return final_str

    def request_set_data(self):

        # request 이므로, 입력부분이 NICE인 경우에는 공백으로 채워준다.
        self._fill_nice_blank(self.enum_class_data)

        # 현재 DB값을 가져와서 enum에 넣어준다.
        self._set_DBvalue_to_enum(self.enum_class_data)

        # default value를 세팅해준다.
        self._set_default_value_to_final(self.enum_class_data)

        # self.enum_class_data.customer_create_data_25.value.final_value = 'Y'
        # self.enum_class_data.customer_create_data_26.value.final_value = 'D000'
        # self.enum_class_data.customer_create_data_27.value.final_value = "정상처리되었습니다."

        # 나머지 blank를 채워준다.
        self._fill_blank(self.enum_class_data)

        self._show_enum_value(self.enum_class_data)

        # string을 생성한다.
        final_str = self.create_string(self.enum_class_data)
        print("final_len {} \n final_str {}".format(len(final_str), final_str))

        return final_str


    def request_set_tail(self):
        self._fill_nice_blank(self.enum_class_tail)
        self._set_default_value_to_final(self.enum_class_tail)
        self._fill_blank(self.enum_class_tail)
        final_str = self.create_string(self.enum_class_tail)
        self._show_enum_value(self.enum_class_tail)

        return final_str


    def request_process(self):
        fin_str = []
        fin_str.append(self.request_set_header())
        fin_str.append(self.request_set_data())
        fin_str.append(self.request_set_tail())
        str = ''.join([data for data in fin_str])
        file_name = self._get_date() + '.' + self.id + '.MEM'
        file_path = os.path.join(self.path.CMS_SEND_URL, file_name) # file_path 설정해주기
        f = open(file_path, 'w')
        f.write(self.request_set_header())
        # f.write(self.request_set_data())
        f.close()



        pass

    def response_set_header(self):
        pass

    def response_set_data(self):
        pass

    def response_set_tail(self):
        pass

    def response_process(self):
        pass