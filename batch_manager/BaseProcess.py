
"""
customer와 transfer의 기초토대가 되는 parent class
공통적으로 사용되는 method를 정의함.
"""

from enum import Enum
from abc import ABCMeta
import abc
from thebill_settings import path
from thebill_database.database.settings import config
from datetime import datetime



class BaseProcess(metaclass=ABCMeta):

    def __init__(self):
        pass

    def __init__(self, service_dict, service_type):
        self.id = config.ID
        self.apikey = config.APIKEY
        self.database = service_dict["database"]
        self.enum_class = service_dict["enum"]
        self.enum_class_head = self.enum_class.Head
        self.enum_class_data = self.enum_class.Data
        self.enum_class_tail = self.enum_class.Tail
        self.service_type = service_type
        self.data_list = []
        for number in service_dict["list"]:
            query_result = self.database.query.filter(self.database.연번==number).one()
            query_dict = self._query_result_to_dict(query_result)
            self.data_list.append(query_result)
        print(self.data_list)
        # self.database_dict = self._query_result_to_dict(database)
        self.path = path

    def __str__(self):
        return "database{} enum_class {} service_type {}".format(self.database, self.enum_class_head, self.service_type)

    def _get_date(self):
        return datetime.today().strftime('%Y%m%d')

    def _query_result_to_dict(self, cls):
        db_dict = cls.__dict__
        tmp_dict = {}
        del db_dict["_sa_instance_state"]
        for key in db_dict.keys():
            if db_dict[key] != None:
                tmp_dict[key] = db_dict[key]
        return tmp_dict

    # 회원아이디 세팅을 하나 넣어줘야 함.


    def _fill_blank(self, enum_objects):
        for enum in enum_objects:
            enum.value._fill_blank()

    def _fill_nice_blank(self, enum_objects):
        for enum in enum_objects:
            print(enum)
            print(type(enum.value))
            enum.value.set_value_from_service_type()

    def _set_default_value_to_final(self, enum_objects):
        for enum in enum_objects:
            enum.value.set_default_value()

    def _set_DBvalue_to_enum(self, enum_objects):
        for enum in enum_objects:
            if enum.value.column_name in self.database_dict:
                enum.value.final_value = self.database_dict[enum.value.column_name]




    def _show_db_value(self):
        print(self.database_dict)

    def create_string(self, enum_objects):
        return ''.join([enum.value.final_value for enum in enum_objects])


    def _show_enum_value(self, enum_objects):
        for enum in enum_objects:
            print("Column : {} value : {}".format(enum.value.column_name,enum.value.final_value))

    @abc.abstractmethod
    def request_set_header(self):
        pass

    @abc.abstractmethod
    def request_set_data(self):
        pass

    @abc.abstractmethod
    def request_set_tail(self):
        pass

    @abc.abstractmethod
    def request_process(self):
        pass

    @abc.abstractmethod
    def response_set_header(self):
        pass

    @abc.abstractmethod
    def response_set_data(self):
        pass

    @abc.abstractmethod
    def response_set_tail(self):
        pass

    @abc.abstractmethod
    def response_process(self):
        pass