class BasePath(object):
	# BASE_URL = "D://WebRoot//MIS_NICE//thebill-edi-client"
    BASE_URL = "C://thebill"

class ThebillPath(BasePath):

    cms_url = BasePath.BASE_URL + "/cms"
    ok_url = BasePath.BASE_URL + "/ok"
    SEND_URL = BasePath.BASE_URL + "/data/cms/send"
    SEND_OK_URL = BasePath.BASE_URL +"/data/cms/send_ok"
    TEMP_URL = BasePath.BASE_URL + "/data/cms/temp"
    RECV_URL = BasePath.BASE_URL + "/data/cms/recv"

    def __dict__(self):
        pass


path = ThebillPath()