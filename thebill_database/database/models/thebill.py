#-*- coding: utf-8 -*-
from sqlalchemy import Boolean
from sqlalchemy import Column, Integer, NVARCHAR, ForeignKey, DATETIME
from thebill_database.database import Base
from sqlalchemy.orm import relationship

class Document(Base):
    __tablename__ = "TheBill_증빙파일",
    idx = Column('idx', Integer, autoincrement=True, primary_key=True)
    업체동기화코드 = Column('업체동기화코드', Integer)
    회원ID = Column('회원ID', NVARCHAR(20))
    회사명 = Column("회사명", NVARCHAR(50))
    작업 = Column('작업', NVARCHAR(1))
    이용기관코드 = Column('거래사ID', NVARCHAR(8))
    송신일자 = Column('송신일자', NVARCHAR(8))
    증빙구분 = Column('증빙구분', NVARCHAR(1))
    확장자명 = Column('확장자명', NVARCHAR(4))
    증빙자료크기 = Column('증빙자료크기', Integer)
    원본파일명 = Column('원본파일명', NVARCHAR(36))
    증빙_처리결과 = Column('증빙_처리결과', NVARCHAR(1))
    증빙_결과코드 = Column('증빙_결과코드', NVARCHAR(4))
    증빙_결과메세지 = Column('증빙_결과메세지', NVARCHAR(38))
    # remark = Column('remark', NVARCHAR(500))
    # useflag = Column("useflag", NVARCHAR(10))
    # wdate = Column('wdate', DATETIME)
    # wdater = Column('wdater', NVARCHAR(50))
    # lastupdate = Column('lastupdate', DATETIME)
    # lastupdater = Column('lastupdater', NVARCHAR(50))
    # HIT = Column('HIT', Integer)
    # IP = Column('IP', NVARCHAR(20))

class Customer(Base):
    __tablename__ = "TheBill_고객정보DB"
    idx = Column('idx', Integer, autoincrement=True)
    업체동기화코드 = Column('업체동기화코드', Integer, primary_key=True)
    회사명 = Column("회사명", NVARCHAR(50))
    거래사구분 = Column('거래사구분', NVARCHAR(11))
    회원ID = Column('회원ID', NVARCHAR(20), primary_key=True)
    회원명 = Column('회원명', NVARCHAR(20), primary_key=True)
    은행코드 = Column('은행코드', NVARCHAR(3))
    출금은행코드 = Column('출금은행코드', NVARCHAR(16))
    예금주_성명 = Column('예금주_성명', NVARCHAR(20))
    예금주_주민번호 = Column('예금주_주민번호', NVARCHAR(13))
    연락처 = Column('연락처', NVARCHAR(16))
    이메일주소 = Column('이메일주소', NVARCHAR(30))
    상품명 = Column('상품명', NVARCHAR(30))
    카드번호 = Column('카드번호', NVARCHAR(16))
    유효년월 = Column('유효년월', NVARCHAR(4))
    현금영수증인증번호 = Column('현금영수증인증번호', NVARCHAR(20))
    서비스코드 = Column('서비스코드', NVARCHAR(1))
    생성여부 = Column('생성여부', NVARCHAR(1))
    remark = Column('remark', NVARCHAR(500))
    useflag = Column("useflag", NVARCHAR(10))
    wdate = Column('wdate', DATETIME)
    wdater = Column('wdater', NVARCHAR(50))
    lastupdate = Column('lastupdate', DATETIME)
    lastupdater = Column('lastupdater', NVARCHAR(50))
    HIT = Column('HIT', Integer)
    IP = Column('IP', NVARCHAR(20))

    # Delete_customer = relationship("Delete_customer", back_populates="Customer")
    # Modify_customer = relationship("Modify_customer", back_populates="Customer")
    # Create_transfer = relationship("Create_transfer", back_populates="Customer")
    #

    def __repr__(self):
        return "<TheBill_고객정보DB(회원ID='%s',회원명='%s')" % (
            self.회원ID, self.회원명)

class Create_customer(Base):  # from Exchanges to Currency (one to many)
    __tablename__ = "TheBill_고객정보생성"
    #     exchange_id = Column(Integer, autoincrement=True, unique_key=True)
    # 포린키를 걸거나 말거나
    업체동기화코드 = Column('업체동기화코드', Integer)
    거래사구분 = Column('거래사구분', NVARCHAR(10))
    회원ID = Column('회원ID', NVARCHAR(20))
    회원명 = Column('회원명', NVARCHAR(20))
    연번 = Column('연번', Integer, autoincrement=True, primary_key=True)
    is생성완료 = Column('is생성완료', NVARCHAR(1))
    은행코드 = Column('은행코드', NVARCHAR(3))
    출금계좌번호 = Column('출금계좌번호', NVARCHAR(16))
    예금주_성명 = Column('예금주_성명', NVARCHAR(20))
    예금주_주민번호 = Column('예금주_주민번호', NVARCHAR(13))
    연락처 = Column('연락처', NVARCHAR(16))
    이메일주소 = Column('이메일주소', NVARCHAR(30))
    상품명 = Column('상품명', NVARCHAR(30))
    카드번호 = Column('카드번호', NVARCHAR(16))
    유효년월 = Column('유효년월', NVARCHAR(4))
    현금영수증인증번호 = Column('현금영수증인증번호', NVARCHAR(20))
    서비스코드 = Column('서비스코드', NVARCHAR(1))
    헤더_처리결과 = Column('헤더_처리결과', NVARCHAR(1))
    헤더_결과코드 = Column('헤더_결과코드', NVARCHAR(4))
    헤더_결과메세지 = Column('헤더_결과메세지', NVARCHAR(30))
    데이터_처리결과 = Column('데이터_처리결과', NVARCHAR(1))
    데이터_결과코드 = Column('데이터_결과코드', NVARCHAR(4))
    데이터_결과메세지 = Column('데이터_결과메세지', NVARCHAR(30))
    테일_처리결과 = Column('테일_처리결과', NVARCHAR(1))
    테일_결과코드 = Column('테일_결과코드', NVARCHAR(4))
    테일_결과메시지 = Column('테일_결과메시지', NVARCHAR(30))
    remark = Column('remark', NVARCHAR(500))
    useflag = Column("useflag", NVARCHAR(10))
    wdate = Column('wdate', DATETIME)
    wdater = Column('wdater', NVARCHAR(50))
    lastupdate = Column('lastupdate', DATETIME)
    lastupdater = Column('lastupdater', NVARCHAR(50))
    HIT = Column('HIT', Integer)
    IP = Column('IP', NVARCHAR(20))

    def __repr__(self):
        return "<TheBill_고객정보생성(회원ID='%s',회원명='%s')" % (
            self.회원ID, self.회원명)

class Delete_customer(Base):
    __tablename__ = "TheBill_고객정보삭제"
    업체동기화코드 = Column('업체동기화코드', Integer, ForeignKey('TheBill_고객정보DB.업체동기화코드'))
    회사명 = Column("회사명", NVARCHAR(50))
    거래사구분 = Column('거래사구분', NVARCHAR(10))
    회원ID = Column('회원ID', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원ID'))
    회원명 = Column('회원명', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원명'))
    연번 = Column('연번', Integer, autoincrement=True, primary_key=True)
    is생성완료 = Column('is생성완료', NVARCHAR(1))
    서비스코드 = Column('서비스코드', NVARCHAR(1))
    헤더_처리결과 = Column('헤더_처리결과', NVARCHAR(1))
    헤더_결과코드 = Column('헤더_결과코드', NVARCHAR(4))
    헤더_결과메세지 = Column('헤더_결과메세지', NVARCHAR(30))
    데이터_처리결과 = Column('데이터_처리결과', NVARCHAR(1))
    데이터_결과코드 = Column('데이터_결과코드', NVARCHAR(4))
    데이터_결과메세지 = Column('데이터_결과메세지', NVARCHAR(30))
    테일_처리결과 = Column('테일_처리결과', NVARCHAR(1))
    테일_결과코드 = Column('테일_결과코드', NVARCHAR(4))
    테일_결과메시지 = Column('테일_결과메시지', NVARCHAR(30))
    remark = Column('remark', NVARCHAR(500))
    useflag = Column("useflag", NVARCHAR(10))
    wdate = Column('wdate', DATETIME)
    wdater = Column('wdater', NVARCHAR(50))
    lastupdate = Column('lastupdate', DATETIME)
    lastupdater = Column('lastupdater', NVARCHAR(50))
    HIT = Column('HIT', Integer)
    IP = Column('IP', NVARCHAR(20))

    # customer = relationship("Customer", foreign_keys=[회원ID,회원명])
    # customer_name = relationship("Customer", foreign_keys=[회원명])


    def __repr__(self):
        return "<TheBill_고객정보삭제(회원ID='%s',회원명='%s')" % (
            self.회원ID, self.회원명)

class Modify_customer(Base):
    __tablename__ = "TheBill_고객정보수정"
    업체동기화코드 = Column('업체동기화코드', Integer, ForeignKey('TheBill_고객정보DB.업체동기화코드'))
    회사명 = Column("회사명", NVARCHAR(50))
    거래사구분 = Column('거래사구분', NVARCHAR(10))
    회원ID = Column('회원ID', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원ID'))
    회원명 = Column('회원명', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원명'))
    연번 = Column('연번', Integer, autoincrement=True, primary_key=True)
    is생성완료 = Column('is생성완료', NVARCHAR(1))
    은행코드 = Column('은행코드', NVARCHAR(3))
    출금계좌번호 = Column('출금계좌번호', NVARCHAR(16))
    예금주_성명 = Column('예금주_성명', NVARCHAR(20))
    예금주_주민번호 = Column('예금주_주민번호', NVARCHAR(13))
    연락처 = Column('연락처', NVARCHAR(16))
    이메일주소 = Column('이메일주소', NVARCHAR(30))
    상품명 = Column('상품명', NVARCHAR(30))
    카드번호 = Column('카드번호', NVARCHAR(16))
    유효년월 = Column('유효년월', NVARCHAR(4))
    현금영수증인증번호 = Column('현금영수증인증번호', NVARCHAR(20))
    서비스코드 = Column('서비스코드', NVARCHAR(1))
    헤더_처리결과 = Column('헤더_처리결과', NVARCHAR(1))
    헤더_결과코드 = Column('헤더_결과코드', NVARCHAR(4))
    헤더_결과메세지 = Column('헤더_결과메세지', NVARCHAR(30))
    데이터_처리결과 = Column('데이터_처리결과', NVARCHAR(1))
    데이터_결과코드 = Column('데이터_결과코드', NVARCHAR(4))
    데이터_결과메세지 = Column('데이터_결과메세지', NVARCHAR(30))
    테일_처리결과 = Column('테일_처리결과', NVARCHAR(1))
    테일_결과코드 = Column('테일_결과코드', NVARCHAR(4))
    테일_결과메시지 = Column('테일_결과메시지', NVARCHAR(30))
    remark = Column('remark', NVARCHAR(500))
    useflag = Column("useflag", NVARCHAR(10))
    wdate = Column('wdate', DATETIME)
    wdater = Column('wdater', NVARCHAR(50))
    lastupdate = Column('lastupdate', DATETIME)
    lastupdater = Column('lastupdater', NVARCHAR(50))
    HIT = Column('HIT', Integer)
    IP = Column('IP', NVARCHAR(20))


    def __repr__(self):
        return "<TheBill_고객정보수정(회원ID='%s',회원명='%s')" % (
            self.회원ID, self.회원명)


class Create_transfer(Base):
    __tablename__ = "TheBill_출금신청및조회"
    # midx = Column('midx', Integer)
    업체동기화코드 = Column('업체동기화코드', Integer, ForeignKey('TheBill_고객정보DB.업체동기화코드'))
    회사명 = Column("회사명", NVARCHAR(50))
    거래사구분 = Column('거래사구분', NVARCHAR(20))
    회원ID = Column('회원ID', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원ID'))
    회원명 = Column('회원명', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원명'))
    근거연번 = Column('근거연번', Integer) # 포린키 달아야함.
    연번 = Column('연번', Integer, autoincrement=True, primary_key=True)
    처리내역 = Column('처리내역', NVARCHAR(1))
    통장기재내역 = Column('통장기재내역', NVARCHAR(2))
    출금신청금액 = Column('출금신청금액', Integer)
    현금영수증발행여부 = Column('현금영수증발행여부', NVARCHAR(1))
    서비스코드 = Column('서비스코드', NVARCHAR(1))
    실제출금일 = Column('실제출금일', NVARCHAR(8))
    실제출금금액 = Column('실제출금금액', Integer)
    출금수수료 = Column('출금수수료', Integer)
    승인번호 = Column('승인번호', NVARCHAR(10))
    헤더_처리결과 = Column('헤더_처리결과', NVARCHAR(1))
    헤더_결과코드 = Column('헤더_결과코드', NVARCHAR(4))
    헤더_결과메세지 = Column('헤더_결과메세지', NVARCHAR(30))
    데이터_처리결과 = Column('데이터_처리결과', NVARCHAR(1))
    데이터_결과코드 = Column('데이터_결과코드', NVARCHAR(4))
    데이터_결과메세지 = Column('데이터_결과메세지', NVARCHAR(30))
    테일_처리결과 = Column('테일_처리결과', NVARCHAR(1))
    테일_결과코드 = Column('테일_결과코드', NVARCHAR(4))
    테일_결과메시지 = Column('테일_결과메시지', NVARCHAR(30))
    remark = Column('remark', NVARCHAR(500))
    useflag = Column("useflag", NVARCHAR(10))
    wdate = Column('wdate', DATETIME)
    wdater = Column('wdater', NVARCHAR(50))
    lastupdate = Column('lastupdate', DATETIME)
    lastupdater = Column('lastupdater', NVARCHAR(50))
    HIT = Column('HIT', Integer)
    IP = Column('IP', NVARCHAR(20))



    def __repr__(self):
        return "<TheBill_출금신청및조회(회원ID='%s',회원명='%s')" % (
            self.회원ID, self.회원명)

class TransferInfo(Base):
    __tablename__ = "TheBill_출금정보"
    연번 = Column('idx', Integer, autoincrement=True, primary_key=True)
    업체동기화코드 = Column('업체동기화코드', Integer, ForeignKey('TheBill_고객정보DB.업체동기화코드'))
    회사명 = Column("회사명", NVARCHAR(50))
    거래사구분 = Column('거래사구분', NVARCHAR(20))
    회원ID = Column('회원ID', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원ID'))
    회원명 = Column('회원명', NVARCHAR(20), ForeignKey('TheBill_고객정보DB.회원명'))
    처리내역 = Column('처리내역', NVARCHAR(1))
    통장기재내역 = Column('통장기재내역', NVARCHAR(2))
    출금신청금액 = Column('출금신청금액', Integer)
    현금영수증발행여부 = Column('현금영수증발행여부', NVARCHAR(1))
    서비스코드 = Column('서비스코드', NVARCHAR(1))
    실제출금일 = Column('실제출금일', NVARCHAR(8))
    실제출금금액 = Column('실제출금금액', Integer)
    출금수수료 = Column('출금수수료', Integer)
    승인번호 = Column('승인번호', NVARCHAR(10))
    출금시작일 = Column('출금시작일', NVARCHAR(10))
    출금요청일 = Column('출금요청일', NVARCHAR(2))
    출금종료일 = Column("출금종료일", NVARCHAR(10))
    출금횟수 = Column('출금횟수', NVARCHAR(2))
    remark = Column('remark', NVARCHAR(500))
    useflag = Column("useflag", NVARCHAR(10))
    wdate = Column('wdate', DATETIME)
    wdater = Column('wdater', NVARCHAR(50))
    lastupdate = Column('lastupdate', DATETIME)
    lastupdater = Column('lastupdater', NVARCHAR(50))
    HIT = Column('HIT', Integer)
    IP = Column('IP', NVARCHAR(20))

    def __repr__(self):
        return "<TheBill_출금정보(회원ID='%s',회원명='%s')" % (
            self.회원ID, self.회원명)



# class Upbit(Base):
# 	__tablename__ = "upbit_pair"
# 	original_symbol = Column(NVARCHAR(40), nullable=False)
# 	symbol = Column(NVARCHAR(40), ForeignKey('cryptomarket.crypto_pair'), primary_key=True)
#
# class Currency(Base):
# 	__tablename__ = "currency"
# 	#     currency_id = Column(Integer, nullable=False)
# 	currency_name = Column(NVARCHAR(50), nullable=False)
# 	currency_symbol = Column(NVARCHAR(50), primary_key=True)
# 	is_fiat = Column(Boolean, nullable=False)
#
# 	#     BaseCryptoMarket = relationship("CryptoMarket", back_populates="BaseCurrency")
# 	#     QuoteCryptoMarket = relationship("CryptoMarket", back_populates="QuoteCurrency")
#
# 	def __repr__(self):
# 		return "<Currency(currency_name='%s', currency_symbol='%s', is_fiat='%s')>" % (
# 			self.currency_name, self.currency_symbol, self.is_fiat)