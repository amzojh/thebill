"""foreignkey 추가

Revision ID: a149edb5c378
Revises: 060889d4986e
Create Date: 2018-08-10 15:32:56.055588

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a149edb5c378'
down_revision = '060889d4986e'
branch_labels = None
depends_on = None


def upgrade():

    op.create_foreign_key("fk_customer_delete", source_table="TheBill_고객정보삭제", referent_table="TheBill_고객정보DB", local_cols=["회원명","회원ID","업체동기화코드"], remote_cols=["회원명","회원ID", "업체동기화코드"])
    op.create_foreign_key("fk_customer_modify", source_table="TheBill_고객정보수정", referent_table="TheBill_고객정보DB", local_cols=["회원명","회원ID", "업체동기화코드"], remote_cols=["회원명","회원ID", "업체동기화코드"])
    op.create_foreign_key("fk_customer_transfer", source_table="TheBill_출금신청및조회", referent_table="TheBill_고객정보DB", local_cols=["회원명","회원ID", "업체동기화코드"], remote_cols=["회원명","회원ID", "업체동기화코드"])
    op.create_foreign_key("fk_transfer_info", source_table="TheBill_출금정보", referent_table="TheBill_고객정보DB", local_cols=["회원명","회원ID", "업체동기화코드"], remote_cols=["회원명","회원ID", "업체동기화코드"])
    pass


def downgrade():
    op.drop_constraint('fk_customer_delete', "TheBill_고객정보삭제")
    op.drop_constraint('fk_customer_modify', "TheBill_고객정보수정")
    op.drop_constraint('fk_customer_transfer', "TheBill_출금신청및조회")
    op.drop_constraint('fk_transfer_info', "TheBill_출금정보")
    pass
