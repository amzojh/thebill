"""create tables

Revision ID: 2dc84db00590
Revises: 
Create Date: 2018-08-03 11:33:46.096013

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2dc84db00590'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "TheBill_고객정보DB",
        sa.Column('회사idx', sa.Integer),
        sa.Column('거래사구분', sa.NVARCHAR(10)),
        sa.Column('회원ID', sa.NVARCHAR(20), primary_key=True),
        sa.Column('회원명', sa.NVARCHAR(20), primary_key=True),
        sa.Column('은행코드', sa.NVARCHAR(3)),
        sa.Column('출금은행코드', sa.NVARCHAR(16)),
        sa.Column('예금주_성명', sa.NVARCHAR(20)),
        sa.Column('예금주_주민번호', sa.NVARCHAR(13)),
        sa.Column('연락처', sa.NVARCHAR(16)),
        sa.Column('이메일주소', sa.NVARCHAR(30)),
        sa.Column('상품명', sa.NVARCHAR(30)),
        sa.Column('카드번호', sa.NVARCHAR(16)),
        sa.Column('유효년월', sa.NVARCHAR(4)),
        sa.Column('현금영수증인증번호', sa.NVARCHAR(20)),
        sa.Column('서비스코드', sa.NVARCHAR(1)),
        sa.Column('생성여부', sa.NVARCHAR(1)),
    )
    op.create_table(
        'TheBill_고객정보생성',
        sa.Column('회사idx', sa.Integer),
        sa.Column('거래사구분', sa.NVARCHAR(10)),
        sa.Column('회원ID', sa.NVARCHAR(20)),
        sa.Column('회원명', sa.NVARCHAR(20)),
        sa.Column('연번', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('처리내역', sa.NVARCHAR(1)),
        sa.Column('은행코드', sa.NVARCHAR(3)),
        sa.Column('출금은행코드', sa.NVARCHAR(16)),
        sa.Column('예금주_성명', sa.NVARCHAR(20)),
        sa.Column('예금주_주민번호', sa.NVARCHAR(13)),
        sa.Column('연락처', sa.NVARCHAR(16)),
        sa.Column('이메일주소', sa.NVARCHAR(30)),
        sa.Column('상품명', sa.NVARCHAR(30)),
        sa.Column('카드번호', sa.NVARCHAR(16)),
        sa.Column('유효년월', sa.NVARCHAR(4)),
        sa.Column('현금영수증인증번호', sa.NVARCHAR(20)),
        sa.Column('서비스코드', sa.NVARCHAR(1)),
        sa.Column('헤더_처리결과', sa.NVARCHAR(1)),
        sa.Column('헤더_결과코드', sa.NVARCHAR(4)),
        sa.Column('헤더_결과메세지', sa.NVARCHAR(30)),
        sa.Column('데이터_처리결과', sa.NVARCHAR(1)),
        sa.Column('데이터_결과코드', sa.NVARCHAR(4)),
        sa.Column('데이터_결과메세지', sa.NVARCHAR(30)),
        sa.Column('테일_처리결과', sa.NVARCHAR(1)),
        sa.Column('테일_결과코드', sa.NVARCHAR(4)),
        sa.Column('테일_결과메시지', sa.NVARCHAR(30)),
    )
    pass

def downgrade():
    op.drop_table('TheBill_고객정보생성')
    op.drop_table('TheBill_고객정보DB')
    pass
