"""thebill idx 추가

Revision ID: 2448b69bb6f8
Revises: 0e580b3c2a17
Create Date: 2018-08-10 11:02:58.461319

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import ForeignKey, NVARCHAR, Integer, DATETIME, Column


# revision identifiers, used by Alembic.
revision = '2448b69bb6f8'
down_revision = '0e580b3c2a17'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("TheBill_증빙파일") as batch_op:
        batch_op.add_column(Column('업체동기화코드', Integer, nullable=False))
        # batch_op.add_column(Column('remark', NVARCHAR(500)))
        # batch_op.add_column(Column("useflag", NVARCHAR(10)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdate', NVARCHAR(50)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdater', NVARCHAR(50)))
        # batch_op.add_column(Column('HIT', Integer))
        # batch_op.add_column(Column('IP', NVARCHAR(20)))

    with op.batch_alter_table("TheBill_고객정보DB") as batch_op:
        batch_op.add_column(Column('업체동기화코드', Integer, nullable=False))
        # batch_op.add_column(Column('remark', NVARCHAR(500)))
        # batch_op.add_column(Column("useflag", NVARCHAR(10)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdate', NVARCHAR(50)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdater', NVARCHAR(50)))
        # batch_op.add_column(Column('HIT', Integer))
        # batch_op.add_column(Column('IP', NVARCHAR(20)))

    with op.batch_alter_table("TheBill_고객정보생성") as batch_op:
        batch_op.add_column(Column('업체동기화코드', Integer, nullable=False))
        # batch_op.add_column(Column('remark', NVARCHAR(500)))
        # batch_op.add_column(Column("useflag", NVARCHAR(10)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdate', NVARCHAR(50)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdater', NVARCHAR(50)))
        # batch_op.add_column(Column('HIT', Integer))
        # batch_op.add_column(Column('IP', NVARCHAR(20)))

    with op.batch_alter_table("TheBill_고객정보수정") as batch_op:
        batch_op.add_column(Column('업체동기화코드', Integer, nullable=False))
        # batch_op.add_column(Column('remark', NVARCHAR(500)))
        # batch_op.add_column(Column("useflag", NVARCHAR(10)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdate', NVARCHAR(50)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdater', NVARCHAR(50)))
        # batch_op.add_column(Column('HIT', Integer))
        # batch_op.add_column(Column('IP', NVARCHAR(20)))

    with op.batch_alter_table("TheBill_고객정보삭제") as batch_op:
        batch_op.add_column(Column('업체동기화코드', Integer, nullable=False))
        # batch_op.add_column(Column('remark', NVARCHAR(500)))
        # batch_op.add_column(Column("useflag", NVARCHAR(10)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdate', NVARCHAR(50)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdater', NVARCHAR(50)))
        # batch_op.add_column(Column('HIT', Integer))
        # batch_op.add_column(Column('IP', NVARCHAR(20)))


    with op.batch_alter_table("TheBill_출금신청및조회") as batch_op:
        batch_op.add_column(Column('업체동기화코드', Integer, nullable=False))
        # batch_op.add_column(Column('remark', NVARCHAR(500)))
        # batch_op.add_column(Column("useflag", NVARCHAR(10)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdate', NVARCHAR(50)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdater', NVARCHAR(50)))
        # batch_op.add_column(Column('HIT', Integer))
        # batch_op.add_column(Column('IP', NVARCHAR(20)))

    with op.batch_alter_table("TheBill_출금정보") as batch_op:
        batch_op.add_column(Column('업체동기화코드', Integer, nullable=False))
        batch_op.drop_column("회원명")
        batch_op.drop_column("회원ID")


    op.add_column("TheBill_출금정보", Column('회원ID', NVARCHAR(20),nullable=False))
    op.add_column("TheBill_출금정보", Column('회원명', NVARCHAR(20), nullable=False))
    # op.create_table(
    #     "TheBill_증빙파일",
    #     sa.Column('업체동기화코드', Integer),
    #     sa.Column("회사명", NVARCHAR(50)),
    #     sa.Column('거래사구분', NVARCHAR(20)),
    #     sa.Column('회원ID', NVARCHAR(20), primary_key=True),
    #     sa.Column('회원명', NVARCHAR(20), primary_key=True),
    #     sa.Column('처리내역', NVARCHAR(1)),
    #     sa.Column('통장기재내역', NVARCHAR(2)),
    #     sa.Column('출금신청금액', Integer),
    #     sa.Column('현금영수증발행여부', NVARCHAR(1)),
    #     sa.Column('서비스코드', NVARCHAR(1)),
    #     sa.Column('실제출금일', NVARCHAR(8)),
    #     sa.Column('실제출금금액', Integer),
    #     sa.Column('출금수수료', Integer),
    #     sa.Column('승인번호', NVARCHAR(10)),
    #     sa.Column('출금시작일', NVARCHAR(10)),
    #     sa.Column('출금요청일', NVARCHAR(2)),
    #     sa.Column("출금종료일", NVARCHAR(10)),
    #     sa.Column('출금횟수', NVARCHAR(2)),
    #     sa.Column('remark', NVARCHAR(500)),
    #     sa.Column("useflag", NVARCHAR(10)),
    #     sa.Column('wdate', DATETIME),
    #     sa.Column('lastupdate', NVARCHAR(50)),
    #     sa.Column('wdate', DATETIME),
    #     sa.Column('lastupdater', NVARCHAR(50)),
    #     sa.Column('HIT', Integer),
    #     sa.Column('IP', NVARCHAR(20)),
    # )


    # op.drop_constraint('fk_customer_delete', "TheBill_고객정보삭제")
    # op.drop_constraint('fk_customer_modify', "TheBill_고객정보수정")
    # op.drop_constraint('fk_customer_transfer', "TheBill_출금신청및조회")
    # op.drop_constraint("pk_")
    op.create_primary_key(
        "pk_tranfer_info", "TheBill_출금정보",
        ["회원명", "회원ID", "업체동기화코드"]
    )


    # op.create_foreign_key("fk_customer_delete", source_table="TheBill_고객정보삭제", referent_table="TheBill_고객정보DB", local_cols=["회원ID","회원명","업체동기화코드"], remote_cols=["회원ID","회원명", "업체동기화코드"])
    # op.create_foreign_key("fk_customer_modify", source_table="TheBill_고객정보수정", referent_table="TheBill_고객정보DB", local_cols=["회원ID","회원명", "업체동기화코드"], remote_cols=["회원ID","회원명", "업체동기화코드"])
    # op.create_foreign_key("fk_customer_transfer", source_table="TheBill_출금신청및조회", referent_table="TheBill_고객정보DB", local_cols=["회원ID","회원명", "업체동기화코드"], remote_cols=["회원ID","회원명", "업체동기화코드"])
    # op.create_foreign_key("fk_transfer_info", source_table="TheBill_증빙파일", referent_table="TheBill_고객정보DB", local_cols=["회원ID","회원명", "업체동기화코드"], remote_cols=["회원ID","회원명", "업체동기화코드"])

    pass


def downgrade():
    # op.drop_table('TheBill_증빙파일')
    # op.drop_constraint('fk_customer_delete', "TheBill_고객정보삭제")
    # op.drop_constraint('fk_customer_modify', "TheBill_고객정보수정")
    # op.drop_constraint('fk_customer_transfer', "TheBill_출금신청및조회")


    with op.batch_alter_table("TheBill_증빙파일") as batch_op:
        batch_op.drop_column('업체동기화코드')
        # batch_op.add_column('remark')
        # batch_op.add_column("useflag")
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdate')
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdater')
        # batch_op.add_column('HIT')
        # batch_op.add_column('IP')

    with op.batch_alter_table("TheBill_고객정보DB") as batch_op:
        batch_op.drop_column('업체동기화코드')
        # batch_op.add_column('remark')
        # batch_op.add_column("useflag")
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdate')
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdater')
        # batch_op.add_column('HIT')
        # batch_op.add_column('IP')

    with op.batch_alter_table("TheBill_고객정보생성") as batch_op:
        batch_op.drop_column('업체동기화코드')
        # batch_op.add_column('remark')
        # batch_op.add_column("useflag")
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdate')
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdater')
        # batch_op.add_column('HIT')
        # batch_op.add_column('IP')

    with op.batch_alter_table("TheBill_고객정보수정") as batch_op:
        batch_op.drop_column('업체동기화코드')
        # batch_op.add_column('remark')
        # batch_op.add_column("useflag")
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdate')
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdater')
        # batch_op.add_column('HIT')
        # batch_op.add_column('IP')

    with op.batch_alter_table("TheBill_고객정보삭제") as batch_op:
        batch_op.drop_column('업체동기화코드')
        # batch_op.add_column('remark')
        # batch_op.add_column("useflag")
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdate')
        # batch_op.add_column('wdate')
        # batch_op.add_column('lastupdater')
        # batch_op.add_column('HIT')
        # batch_op.add_column('IP')


    with op.batch_alter_table("TheBill_출금신청및조회") as batch_op:
        batch_op.drop_column('업체동기화코드')
        # batch_op.add_column(Column('remark', NVARCHAR(500)))
        # batch_op.add_column(Column("useflag", NVARCHAR(10)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdate', NVARCHAR(50)))
        # batch_op.add_column(Column('wdate', DATETIME))
        # batch_op.add_column(Column('lastupdater', NVARCHAR(50)))
        # batch_op.add_column(Column('HIT', Integer))
        # batch_op.add_column(Column('IP', NVARCHAR(20)))

    pass
