"""key추가

Revision ID: 060889d4986e
Revises: 2448b69bb6f8
Create Date: 2018-08-10 13:19:30.814485

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '060889d4986e'
down_revision = '2448b69bb6f8'
branch_labels = None
depends_on = None


def upgrade():

    op.drop_constraint('fk_customer_delete', "TheBill_고객정보삭제")
    op.drop_constraint('fk_customer_modify', "TheBill_고객정보수정")
    op.drop_constraint('fk_customer_transfer', "TheBill_출금신청및조회")
    op.drop_constraint("pk_customerDB", "TheBill_고객정보DB")
    op.drop_column("TheBill_고객정보DB", "업체동기화코드")
    op.add_column("TheBill_고객정보DB", sa.Column('업체동기화코드', sa.Integer, nullable=False))
    op.create_primary_key(
        "pk_customerDB", "TheBill_고객정보DB",
        ["회원명", "회원ID", "업체동기화코드"]
    )

    pass


def downgrade():
    pass
