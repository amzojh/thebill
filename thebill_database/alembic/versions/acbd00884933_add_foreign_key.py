"""add foreign key

Revision ID: acbd00884933
Revises: ddc61304bd83
Create Date: 2018-08-03 14:28:49.038428

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'acbd00884933'
down_revision = 'ddc61304bd83'
branch_labels = None
depends_on = None


def upgrade():
    # op.drop_column(table_name="TheBill_고객정보삭제", column_name="회원ID")
    # op.drop_column(table_name="TheBill_고객정보삭제", column_name="회원명")
    #
    # op.drop_column(table_name="TheBill_고객정보수정", column_name="회원ID")
    # op.drop_column(table_name="TheBill_고객정보수정", column_name="회원명")
    #
    # op.drop_column(table_name="TheBill_출금신청및조회", column_name="회원ID")
    # op.drop_column(table_name="TheBill_출금신청및조회", column_name="회원명")

    op.create_foreign_key("fk_customer_delete", source_table="TheBill_고객정보삭제", referent_table="TheBill_고객정보DB", local_cols=["회원ID","회원명"], remote_cols=["회원ID","회원명"])
    op.create_foreign_key("fk_customer_modify", source_table="TheBill_고객정보수정", referent_table="TheBill_고객정보DB", local_cols=["회원ID","회원명"], remote_cols=["회원ID","회원명"])
    op.create_foreign_key("fk_customer_transfer", source_table="TheBill_출금신청및조회", referent_table="TheBill_고객정보DB", local_cols=["회원ID","회원명"], remote_cols=["회원ID","회원명"])
    # sa.ForeignKeyConstraint(['회원ID', '회원명'], ['TheBill_고객정보DB.회원ID', 'TheBill_고객정보DB.회원명'], ),
    # sa.ForeignKeyConstraint(['회원ID', '회원명'], ['TheBill_고객정보DB.회원ID', 'TheBill_고객정보DB.회원명'], ),
    # sa.ForeignKeyConstraint(['회원ID', '회원명'], ['TheBill_고객정보DB.회원ID', 'TheBill_고객정보DB.회원명'], ),
    pass


def downgrade():
    op.drop_constraint('fk_customer_delete', "TheBill_고객정보삭제")
    op.drop_constraint('fk_customer_modify', "TheBill_고객정보수정")
    op.drop_constraint('fk_customer_transfer', "TheBill_출금신청및조회")
    pass
