"""add table for 증빙

Revision ID: 0e580b3c2a17
Revises: 563948520173
Create Date: 2018-08-04 21:30:42.441345

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0e580b3c2a17'
down_revision = '563948520173'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "TheBill_증빙파일",
        sa.Column('작업', sa.NVARCHAR(1)),
        sa.Column('이용기관코드', sa.NVARCHAR(8)),
        sa.Column('송신일자', sa.NVARCHAR(8)),
        sa.Column('회원ID', sa.NVARCHAR(20), primary_key=True),
        sa.Column('증빙구분', sa.NVARCHAR(1)),
        sa.Column('확장자명', sa.NVARCHAR(4)),
        sa.Column('증빙자료크기', sa.Integer),
        sa.Column('원본파일명', sa.NVARCHAR(36)),
        sa.Column('증빙_처리결과', sa.NVARCHAR(1)),
        sa.Column('증빙_결과코드', sa.NVARCHAR(4)),
        sa.Column('증빙_결과메시지', sa.NVARCHAR(38))
    )
    #
    # op.drop_constraint('fk_customer_delete', "TheBill_고객정보삭제")
    # op.drop_constraint('fk_customer_modify', "TheBill_고객정보수정")
    # op.drop_constraint('fk_customer_transfer', "TheBill_출금신청및조회")
    #

    pass


def downgrade():
    pass
