"""create table2

Revision ID: ddc61304bd83
Revises: 2dc84db00590
Create Date: 2018-08-03 14:23:17.969201

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ddc61304bd83'
down_revision = '2dc84db00590'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "TheBill_고객정보삭제",
        sa.Column('회사idx', sa.Integer),
        sa.Column('거래사구분', sa.NVARCHAR(10)),
        sa.Column('회원ID', sa.NVARCHAR(20)),
        sa.Column('회원명', sa.NVARCHAR(20)),
        sa.Column('연번', sa.Integer,primary_key=True, autoincrement=True),
        sa.Column('처리내역', sa.NVARCHAR(1)),
        sa.Column('서비스코드', sa.NVARCHAR(1)),
        sa.Column('헤더_처리결과', sa.NVARCHAR(1)),
        sa.Column('헤더_결과코드', sa.NVARCHAR(4)),
        sa.Column('헤더_결과메세지', sa.NVARCHAR(30)),
        sa.Column('데이터_처리결과', sa.NVARCHAR(1)),
        sa.Column('데이터_결과코드', sa.NVARCHAR(4)),
        sa.Column('데이터_결과메세지', sa.NVARCHAR(30)),
        sa.Column('테일_처리결과', sa.NVARCHAR(1)),
        sa.Column('테일_결과코드', sa.NVARCHAR(4)),
        sa.Column('테일_결과메시지', sa.NVARCHAR(30)),
        # sa.ForeignKeyConstraint(['회원ID','회원명'], ['TheBill_고객정보DB.회원ID','TheBill_고객정보DB.회원명'], ),
    )
    op.create_table(
        "TheBill_고객정보수정",
        sa.Column('회사idx', sa.Integer),
        sa.Column('거래사구분', sa.NVARCHAR(10)),
        sa.Column('회원ID', sa.NVARCHAR(20)),
        sa.Column('회원명', sa.NVARCHAR(20)),
        sa.Column('연번', sa.Integer,primary_key=True, autoincrement=True),
        sa.Column('처리내역', sa.NVARCHAR(1)),
        sa.Column('은행코드', sa.NVARCHAR(3)),
        sa.Column('출금은행코드', sa.NVARCHAR(16)),
        sa.Column('예금주_성명', sa.NVARCHAR(20)),
        sa.Column('예금주_주민번호', sa.NVARCHAR(13)),
        sa.Column('연락처', sa.NVARCHAR(16)),
        sa.Column('이메일주소', sa.NVARCHAR(30)),
        sa.Column('상품명', sa.NVARCHAR(30)),
        sa.Column('카드번호', sa.NVARCHAR(16)),
        sa.Column('유효년월', sa.NVARCHAR(4)),
        sa.Column('현금영수증인증번호', sa.NVARCHAR(20)),
        sa.Column('서비스코드', sa.NVARCHAR(1)),
        sa.Column('헤더_처리결과', sa.NVARCHAR(1)),
        sa.Column('헤더_결과코드', sa.NVARCHAR(4)),
        sa.Column('헤더_결과메세지', sa.NVARCHAR(30)),
        sa.Column('데이터_처리결과', sa.NVARCHAR(1)),
        sa.Column('데이터_결과코드', sa.NVARCHAR(4)),
        sa.Column('데이터_결과메세지', sa.NVARCHAR(30)),
        sa.Column('테일_처리결과', sa.NVARCHAR(1)),
        sa.Column('테일_결과코드', sa.NVARCHAR(4)),
        sa.Column('테일_결과메시지', sa.NVARCHAR(30)),
        # sa.ForeignKeyConstraint(['회원ID'], ['TheBill_고객정보DB.회원ID'], ),
        # sa.ForeignKeyConstraint(['회원명'], ['TheBill_고객정보DB.회원명'], ),
    )
    op.create_table(
        "TheBill_출금신청및조회",
        sa.Column('회사idx', sa.Integer),
        sa.Column('거래사구분', sa.NVARCHAR()),
        sa.Column('회원ID', sa.NVARCHAR(20)),
        sa.Column('회원명', sa.NVARCHAR(20)),
        sa.Column('연번', sa.Integer,primary_key=True, autoincrement=True),
        sa.Column('처리내역', sa.NVARCHAR(1)),
        sa.Column('통장기재내역', sa.NVARCHAR(2)),
        sa.Column('출금신청금액', sa.Integer),
        sa.Column('현금영수증발행여부', sa.NVARCHAR(1)),
        sa.Column('서비스코드', sa.NVARCHAR(1)),
        sa.Column('실제출금일', sa.NVARCHAR(8)),
        sa.Column('실제출금금액', sa.Integer),
        sa.Column('출금수수료', sa.Integer),
        sa.Column('승인번호', sa.NVARCHAR(10)),
        sa.Column('헤더_처리결과', sa.NVARCHAR(1)),
        sa.Column('헤더_결과코드', sa.NVARCHAR(4)),
        sa.Column('헤더_결과메세지', sa.NVARCHAR(30)),
        sa.Column('데이터_처리결과', sa.NVARCHAR(1)),
        sa.Column('데이터_결과코드', sa.NVARCHAR(4)),
        sa.Column('데이터_결과메세지', sa.NVARCHAR(30)),
        sa.Column('테일_처리결과', sa.NVARCHAR(1)),
        sa.Column('테일_결과코드', sa.NVARCHAR(4)),
        sa.Column('테일_결과메시지', sa.NVARCHAR(30)),
        # sa.ForeignKeyConstraint(['회원ID'], ['TheBill_고객정보DB.회원ID'], ),
        # sa.ForeignKeyConstraint(['회원명'], ['TheBill_고객정보DB.회원명'], ),
    )
    pass


def downgrade():
    op.drop_table('TheBill_출금신청및조회')
    op.drop_table('TheBill_고객정보삭제')
    op.drop_table('TheBill_고객정보수정')

    pass
