"""alter column name

Revision ID: 563948520173
Revises: acbd00884933
Create Date: 2018-08-04 16:40:46.687108

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '563948520173'
down_revision = 'acbd00884933'
branch_labels = None
depends_on = None


def upgrade():



    op.alter_column(table_name="TheBill_고객정보DB", column_name='출금은행코드', new_column_name="출금계좌번호")
    op.alter_column(table_name="TheBill_고객정보수정", column_name='출금은행코드', new_column_name="출금계좌번호")
    op.alter_column(table_name="TheBill_고객정보생성", column_name='출금은행코드', new_column_name="출금계좌번호")
    pass


def downgrade():
    op.alter_column("TheBill_고객정보DB", column_name='출금계좌번호', new_column_name="출금은행코드")
    op.alter_column("TheBill_고객정보수정", column_name='출금계좌번호', new_column_name="출금은행코드")
    op.alter_column("TheBill_고객정보생성", column_name='출금계좌번호', new_column_name="출금은행코드")
    pass
